
/*==============================================================*/
/* Table: CATEGORIAS                                            */
/*==============================================================*/
create table CATEGORIAS
(
   ID_CATEGORIA         int not null auto_increment  comment '',
   NOMBRE_CATEGORIA     varchar(50) not null  comment '',
   primary key (ID_CATEGORIA)
);

/*==============================================================*/
/* Table: ESTANTERIAS                                           */
/*==============================================================*/
create table ESTANTERIAS
(
   ID_ESTANTERIA        int not null auto_increment  comment '',
   CAPACIDAD_ESTANTERIA int(11) not null  comment '',
   TIPO_ESTANTERIA      varchar(50) not null  comment '',
   POSICION_ESTANTERIA  varchar(50) not null  comment '',
   primary key (ID_ESTANTERIA)
);

/*==============================================================*/
/* Table: FACTURACION                                           */
/*==============================================================*/
create table FACTURACIONES
(
   ID_FACTURACION       int not null auto_increment  comment '',
   ID_VENTA int not null comment '',
   FECHA_FACTURACION    datetime not null  comment '',
   TIPO_FACTURACION     varchar(50) not null  comment '',
   VENDEDOR             varchar(50) not null  comment '',
   COMPRADOR            varchar(50) not null  comment '',
   SUBTOTAL_FACTURACION numeric(10,2) not null  comment '',
   primary key (ID_FACTURACION)
);

/*==============================================================*/
/* Table: PRODUCTOS                                             */
/*==============================================================*/
create table PRODUCTOS
(
   ID_PRODUCTO          int not null auto_increment  comment '',
   ID_CATEGORIA         int  comment '',
   ID_ESTANTERIA        int  comment '',
   NOMBRE_PRODUCTO      varchar(50) not null  comment '',
   COD_BARRAS_PRODUCTO   varchar(50) not null  comment '',
   STOCK_PRODUCTO       int(10) not null  comment '',
   VALORCOMPRA_PRODUCTO numeric(10,2) not null  comment '',
   VALORVENTA_PRODUCTO  numeric(10,2) not null  comment '',
   IMAGEN_PRODUCTO      LONGTEXT,
   primary key (ID_PRODUCTO)
);

/*==============================================================*/
/* Table: VENTAS                                                */
/*==============================================================*/
create table VENTAS
(
   ID_VENTA             int not null auto_increment comment '',
   COD_VENTA            int not null comment '',
   ID_PRODUCTO          int not null  comment '',
   CANTIDAD_PRODUCTOS   int(10) not null  comment '',
   primary key (ID_VENTA)
);

alter table FACTURACIONES add constraint FK_FACTURAC_REFERENCE_VENTAS foreign key (ID_VENTA)
      references VENTAS (ID_VENTA) on delete restrict on update restrict;

alter table PRODUCTOS add constraint FK_PRODUCTO_REFERENCE_CATEGORI foreign key (ID_CATEGORIA)
      references CATEGORIAS (ID_CATEGORIA) on delete restrict on update restrict;

alter table PRODUCTOS add constraint FK_PRODUCTO_REFERENCE_ESTANTER foreign key (ID_ESTANTERIA)
      references ESTANTERIAS (ID_ESTANTERIA) on delete restrict on update restrict;

alter table VENTAS add constraint FK_VENTAS_REFERENCE_PRODUCTO foreign key (ID_PRODUCTO)
      references PRODUCTOS (ID_PRODUCTO) on delete restrict on update restrict;

