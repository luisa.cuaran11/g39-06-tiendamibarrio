package org.mintic.tiendamibarrio.repositorios;

import java.util.List;
import org.mintic.tiendamibarrio.entidades.Estanteria;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface EstanteriaRepositorio extends JpaRepository<Estanteria, Integer>{
    @Query("SELECT est FROM Estanteria est")
    public List<Estanteria> obtenerTodos();
}
