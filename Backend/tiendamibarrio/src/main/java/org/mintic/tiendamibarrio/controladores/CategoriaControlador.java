package org.mintic.tiendamibarrio.controladores;

import java.util.List;
import org.mintic.tiendamibarrio.entidades.Categoria;
import org.mintic.tiendamibarrio.servicios.CategoriaServicios;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/categorias")
@CrossOrigin(origins = "*")
public class CategoriaControlador {

    @Autowired
    private CategoriaServicios tuServicio;

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/listar", method = RequestMethod.GET)
    public List<Categoria> listarMiCategoria() {
        return tuServicio.consultar();
    }
//  localhost:8094/tiendamibarrio/categoria/listar

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/crear", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<Categoria> crearMiCategoria(@RequestBody Categoria objCat) {
        if (tuServicio.agregar(objCat)) {
            return ResponseEntity.ok(objCat);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
//  localhost:8094/tiendamibarrio/productos/crear

    @ResponseStatus(code = HttpStatus.OK, reason = "Odjeto eliminado Correctamente")
    @RequestMapping(value = "/borrar/{codigo}", method = RequestMethod.DELETE)
    public void borrarMiCategoria(@PathVariable Integer codigo) {
        tuServicio.eliminar(codigo);
    }
//  localhost:8094/tiendamibarrio/productos/borrar/cod

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/buscar/{codigo}", method = RequestMethod.GET)
    public Categoria buscarUnaCatoria(@PathVariable Integer codigo) {
        return tuServicio.buscar(codigo);
    }
//  localhost:8094/tiendamibarrio/productos/buscar

    @ResponseStatus(code = HttpStatus.OK, reason = "Objeto Actualizado Correctamente")
    @RequestMapping(value = "/actualizar", method = RequestMethod.PUT)
    public Boolean ActualizarMiProducto(@RequestBody Categoria miObjeto) {
        return tuServicio.actualizar(miObjeto);
    }
//  localhost:8094/tiendamibarrio/productos/actualizar 
}
