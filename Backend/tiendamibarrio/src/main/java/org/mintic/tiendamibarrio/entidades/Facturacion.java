package org.mintic.tiendamibarrio.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "facturaciones")
public class Facturacion implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_FACTURACION")
    private Integer idFacturacion;
    
    @Column(name = "FECHA_FACTURACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaFacturacion;
    
    @Column(name = "TIPO_FACTURACION")
    private String tipoFacturacion;
    
    @Column(name = "VENDEDOR")
    private String vendedor;
    
    @Column(name = "COMPRADOR")
    private String comprador;
    
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "SUBTOTAL_FACTURACION")
    private BigDecimal subtotalFacturacion;
    
    @JoinColumn(name = "ID_VENTA", referencedColumnName = "ID_VENTA")
    @ManyToOne(optional = false)
    private Venta idVenta;

    public Facturacion() {
    }

    public Facturacion(Integer idFacturacion) {
        this.idFacturacion = idFacturacion;
    }

    public Facturacion(Integer idFacturacion, Date fechaFacturacion, String tipoFacturacion, String vendedor, String comprador, BigDecimal subtotalFacturacion) {
        this.idFacturacion = idFacturacion;
        this.fechaFacturacion = fechaFacturacion;
        this.tipoFacturacion = tipoFacturacion;
        this.vendedor = vendedor;
        this.comprador = comprador;
        this.subtotalFacturacion = subtotalFacturacion;
    }

    public Integer getIdFacturacion() {
        return idFacturacion;
    }

    public void setIdFacturacion(Integer idFacturacion) {
        this.idFacturacion = idFacturacion;
    }

    public Date getFechaFacturacion() {
        return fechaFacturacion;
    }

    public void setFechaFacturacion(Date fechaFacturacion) {
        this.fechaFacturacion = fechaFacturacion;
    }

    public String getTipoFacturacion() {
        return tipoFacturacion;
    }

    public void setTipoFacturacion(String tipoFacturacion) {
        this.tipoFacturacion = tipoFacturacion;
    }

    public String getVendedor() {
        return vendedor;
    }

    public void setVendedor(String vendedor) {
        this.vendedor = vendedor;
    }

    public String getComprador() {
        return comprador;
    }

    public void setComprador(String comprador) {
        this.comprador = comprador;
    }

    public BigDecimal getSubtotalFacturacion() {
        return subtotalFacturacion;
    }

    public void setSubtotalFacturacion(BigDecimal subtotalFacturacion) {
        this.subtotalFacturacion = subtotalFacturacion;
    }

    public Venta getIdVenta() {
        return idVenta;
    }

    public void setIdVenta(Venta idVenta) {
        this.idVenta = idVenta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idFacturacion != null ? idFacturacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Facturacion)) {
            return false;
        }
        Facturacion other = (Facturacion) object;
        return !((this.idFacturacion == null && other.idFacturacion != null) || (this.idFacturacion != null && !this.idFacturacion.equals(other.idFacturacion)));
    }

    @Override
    public String toString() {
        return "org.mintic.tiendamibarrio.entidades.Facturaciones[ idFacturacion=" + idFacturacion + " ]";
    }
    
}
