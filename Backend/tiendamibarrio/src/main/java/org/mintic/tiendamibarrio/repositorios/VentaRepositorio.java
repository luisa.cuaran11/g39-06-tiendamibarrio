
package org.mintic.tiendamibarrio.repositorios;

import java.util.List;
import org.mintic.tiendamibarrio.entidades.Venta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface VentaRepositorio extends JpaRepository<Venta, Integer>{
    @Query("SELECT ve FROM Venta ve")
    public List<Venta> obtenerTodos();
}
