package org.mintic.tiendamibarrio.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "productos")
public class Producto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_PRODUCTO")
    private Integer idProducto;
    
    @Column(name = "NOMBRE_PRODUCTO")
    private String nombreProducto;
    
    @Column(name = "COD_BARRAS_PRODUCTO")
    private String codBarrasProducto;
    
    @Column(name = "STOCK_PRODUCTO")
    private int stockProducto;
    
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "VALORCOMPRA_PRODUCTO")
    private BigDecimal valorcompraProducto;
    
    @Column(name = "VALORVENTA_PRODUCTO")
    private BigDecimal valorventaProducto;
    
    @Lob
    @Column(name = "IMAGEN_PRODUCTO")
    private String imagenProducto;
    
    @JoinColumn(name = "ID_CATEGORIA", referencedColumnName = "ID_CATEGORIA")
    @ManyToOne
    private Categoria idCategoria;
    
    @JoinColumn(name = "ID_ESTANTERIA", referencedColumnName = "ID_ESTANTERIA")
    @ManyToOne
    private Estanteria idEstanteria;

    public Producto() {
    }

    public Producto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Producto(Integer idProducto, String nombreProducto, String codBarrasProducto, int stockProducto, BigDecimal valorcompraProducto, BigDecimal valorventaProducto) {
        this.idProducto = idProducto;
        this.nombreProducto = nombreProducto;
        this.codBarrasProducto = codBarrasProducto;
        this.stockProducto = stockProducto;
        this.valorcompraProducto = valorcompraProducto;
        this.valorventaProducto = valorventaProducto;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public String getCodBarrasProducto() {
        return codBarrasProducto;
    }

    public void setCodBarrasProducto(String codBarrasProducto) {
        this.codBarrasProducto = codBarrasProducto;
    }

    public int getStockProducto() {
        return stockProducto;
    }

    public void setStockProducto(int stockProducto) {
        this.stockProducto = stockProducto;
    }

    public BigDecimal getValorcompraProducto() {
        return valorcompraProducto;
    }

    public void setValorcompraProducto(BigDecimal valorcompraProducto) {
        this.valorcompraProducto = valorcompraProducto;
    }

    public BigDecimal getValorventaProducto() {
        return valorventaProducto;
    }

    public void setValorventaProducto(BigDecimal valorventaProducto) {
        this.valorventaProducto = valorventaProducto;
    }

    public String getImagenProducto() {
        return imagenProducto;
    }

    public void setImagenProducto(String imagenProducto) {
        this.imagenProducto = imagenProducto;
    }

    public Categoria getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(Categoria idCategoria) {
        this.idCategoria = idCategoria;
    }

    public Estanteria getIdEstanteria() {
        return idEstanteria;
    }

    public void setIdEstanteria(Estanteria idEstanteria) {
        this.idEstanteria = idEstanteria;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProducto != null ? idProducto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Producto)) {
            return false;
        }
        Producto other = (Producto) object;
        return !((this.idProducto == null && other.idProducto != null) || (this.idProducto != null && !this.idProducto.equals(other.idProducto)));
    }

    @Override
    public String toString() {
        return "org.mintic.tiendamibarrio.entidades.Productos[ idProducto=" + idProducto + " ]";
    }
    
}
