package org.mintic.tiendamibarrio.servicios;

import java.util.List;
import java.util.Optional;
import org.mintic.tiendamibarrio.entidades.Venta;
import org.mintic.tiendamibarrio.interfaces.Operaciones;
import org.mintic.tiendamibarrio.repositorios.VentaRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service("ventaService")
public class VentaServicios implements Operaciones<Venta>{
    
    @Autowired
    private VentaRepositorio tuRepo;

    @Override
    public List<Venta> consultar() {
        return tuRepo.obtenerTodos();
    }

    @Override
    public Boolean agregar(Venta miObjeto) {
        Venta temporal = tuRepo.save(miObjeto);
        return temporal != null;
    }

    @Override
    public Boolean eliminar(Integer llavePrimaria) {
        tuRepo.deleteById(llavePrimaria);
        return !tuRepo.existsById(llavePrimaria);
    }

    @Override
    public Venta buscar(Integer llavePrimaria) {
        return tuRepo.findById(llavePrimaria).get();
    }

    @Override
    public Boolean actualizar(Venta miObjeto) {
        Optional<Venta> objVerificar = tuRepo.findById(miObjeto.getIdVenta());
        if (objVerificar.isPresent()) {
            tuRepo.save(miObjeto);
            return true;
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Objeto no aceptado");
        }
    }

    @Override
    public Integer cantidadRegistros() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    
    
}
