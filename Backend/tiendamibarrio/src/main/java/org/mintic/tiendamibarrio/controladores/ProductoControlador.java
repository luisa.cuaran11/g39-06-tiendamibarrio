package org.mintic.tiendamibarrio.controladores;

import java.util.List;
import org.mintic.tiendamibarrio.entidades.Producto;
import org.mintic.tiendamibarrio.servicios.ProductoServicios;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/productos")
@CrossOrigin(origins = "*")
public class ProductoControlador {
    
    @Autowired
    private ProductoServicios tuServicio;
    
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/listar", method = RequestMethod.GET)
    public List<Producto> listarMiProducto() {
        return tuServicio.consultar();
    }
//  localhost:8094/tiendamibarrio/productos/listar
    
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/crear", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<Producto> crearMiProducto(@RequestBody Producto objTU) {
        if (tuServicio.agregar(objTU)) {
            return ResponseEntity.ok(objTU);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
//  localhost:8094/tiendamibarrio/productos/crear
    
    @ResponseStatus(code = HttpStatus.OK, reason = "Odjeto eliminado Correctamente")
    @RequestMapping(value = "/borrar/{codigo}", method = RequestMethod.DELETE)
    public void borrarMiProducto(@PathVariable Integer codigo) {
        tuServicio.eliminar(codigo);
    }
//  localhost:8094/tiendamibarrio/productos/borrar/cod
    
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/buscar/{codigo}", method = RequestMethod.GET)
    public Producto buscarUnProducto(@PathVariable Integer codigo) {
        return tuServicio.buscar(codigo);
    }
//  localhost:8094/tiendamibarrio/productos/buscar
    
    @ResponseStatus(code = HttpStatus.OK, reason = "Objeto Actualizado Correctamente")
    @RequestMapping(value = "/actualizar", method = RequestMethod.PUT)
    public Boolean ActualizarMiProducto(@RequestBody Producto miObjeto) {
        return tuServicio.actualizar(miObjeto);
    }
//  localhost:8094/tiendamibarrio/productos/actualizar    
    
}

