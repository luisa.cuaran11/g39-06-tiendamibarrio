package org.mintic.tiendamibarrio.servicios;

import java.util.List;
import java.util.Optional;
import org.mintic.tiendamibarrio.entidades.Categoria;
import org.mintic.tiendamibarrio.interfaces.Operaciones;
import org.mintic.tiendamibarrio.repositorios.CategoriaRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service("categoriaService")
public class CategoriaServicios implements Operaciones<Categoria>{
    
    @Autowired
    private CategoriaRepositorio tuRepo;

    @Override
    public List<Categoria> consultar() {
        return tuRepo.obtenerTodos();
    }

    @Override
    public Boolean agregar(Categoria miObjeto) {
        Categoria temporal = tuRepo.save(miObjeto);
        return temporal != null;
    }

    @Override
    public Boolean eliminar(Integer llavePrimaria) {
        tuRepo.deleteById(llavePrimaria);
        return !tuRepo.existsById(llavePrimaria);
    }

    @Override
    public Categoria buscar(Integer llavePrimaria) {
        return tuRepo.findById(llavePrimaria).get();
    }

    @Override
    public Boolean actualizar(Categoria miObjeto) {
        Optional<Categoria> objVerificar = tuRepo.findById(miObjeto.getIdCategoria());
        if (objVerificar.isPresent()) {
            tuRepo.save(miObjeto);
            return true;
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Objeto no aceptado");
        }
    }

    @Override
    public Integer cantidadRegistros() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}
