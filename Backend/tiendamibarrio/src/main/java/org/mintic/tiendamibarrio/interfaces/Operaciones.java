package org.mintic.tiendamibarrio.interfaces;

import java.util.List;

public interface Operaciones<T> {

    public List<T> consultar();

    public Boolean agregar(T miObjeto);

    public Boolean eliminar(Integer llavePrimaria);

    public T buscar(Integer llavePrimaria);

    public Boolean actualizar(T miObjeto);

    public Integer cantidadRegistros();
}
