package org.mintic.tiendamibarrio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TiendamibarrioApplication {

	public static void main(String[] args) {
		SpringApplication.run(TiendamibarrioApplication.class, args);
	}

}
