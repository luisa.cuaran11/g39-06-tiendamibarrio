package org.mintic.tiendamibarrio.servicios;

import java.util.List;
import java.util.Optional;
import org.mintic.tiendamibarrio.entidades.Estanteria;
import org.mintic.tiendamibarrio.interfaces.Operaciones;
import org.mintic.tiendamibarrio.repositorios.EstanteriaRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service("sstanteriaService")
public class EstanteriaServicios implements Operaciones<Estanteria>{
    
    @Autowired
    private EstanteriaRepositorio tuRepo;

    @Override
    public List<Estanteria> consultar() {
        return tuRepo.obtenerTodos();
    }

    @Override
    public Boolean agregar(Estanteria miObjeto) {
        Estanteria temporal = tuRepo.save(miObjeto);
        return temporal != null;
    }
    
    @Override
    public Boolean eliminar(Integer llavePrimaria) {
        tuRepo.deleteById(llavePrimaria);
        return !tuRepo.existsById(llavePrimaria);
    }
    
    @Override
    public Estanteria buscar(Integer llavePrimaria) {
        return tuRepo.findById(llavePrimaria).get();
    }

    @Override
    public Boolean actualizar(Estanteria miObjeto) {
        Optional<Estanteria> objVerificar = tuRepo.findById(miObjeto.getIdEstanteria());
        if (objVerificar.isPresent()) {
            tuRepo.save(miObjeto);
            return true;
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Objeto no aceptado");
        }
    }

    @Override
    public Integer cantidadRegistros() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    
}
