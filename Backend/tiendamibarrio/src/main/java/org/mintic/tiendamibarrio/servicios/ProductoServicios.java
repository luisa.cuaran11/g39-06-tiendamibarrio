package org.mintic.tiendamibarrio.servicios;

import java.util.List;
import java.util.Optional;
import org.mintic.tiendamibarrio.entidades.Producto;
import org.mintic.tiendamibarrio.interfaces.Operaciones;
import org.mintic.tiendamibarrio.repositorios.ProductoRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service("productoService")
public class ProductoServicios implements Operaciones<Producto>{

    @Autowired
    private ProductoRepositorio tuRepo;

    @Override
    public List<Producto> consultar() {
        return tuRepo.obtenerTodos();
    }

    @Override
    public Boolean agregar(Producto miObjeto) {
        Producto temporal = tuRepo.save(miObjeto);
        return temporal != null;
    }
    
        @Override
    public Boolean eliminar(Integer llavePrimaria) {
        tuRepo.deleteById(llavePrimaria);
        return !tuRepo.existsById(llavePrimaria);
    }
    
        @Override
    public Producto buscar(Integer llavePrimaria) {
        return tuRepo.findById(llavePrimaria).get();
    }
    
        @Override
    public Boolean actualizar(Producto miObjeto) {
        Optional<Producto> objVerificar = tuRepo.findById(miObjeto.getIdProducto());
        if (objVerificar.isPresent()) {
            tuRepo.save(miObjeto);
            return true;
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Objeto no aceptado");
        }
    }

    @Override
    public Integer cantidadRegistros() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }


}
