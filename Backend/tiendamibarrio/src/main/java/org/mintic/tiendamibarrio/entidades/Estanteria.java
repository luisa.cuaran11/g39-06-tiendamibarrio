package org.mintic.tiendamibarrio.entidades;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "estanterias")
public class Estanteria implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_ESTANTERIA")
    private Integer idEstanteria;
    
    @Column(name = "CAPACIDAD_ESTANTERIA")
    private int capacidadEstanteria;
    
    @Column(name = "TIPO_ESTANTERIA")
    private String tipoEstanteria;
    
    @Column(name = "POSICION_ESTANTERIA")
    private String posicionEstanteria;

    public Estanteria() {
    }

    public Estanteria(Integer idEstanteria) {
        this.idEstanteria = idEstanteria;
    }

    public Estanteria(Integer idEstanteria, int capacidadEstanteria, String tipoEstanteria, String posicionEstanteria) {
        this.idEstanteria = idEstanteria;
        this.capacidadEstanteria = capacidadEstanteria;
        this.tipoEstanteria = tipoEstanteria;
        this.posicionEstanteria = posicionEstanteria;
    }

    public Integer getIdEstanteria() {
        return idEstanteria;
    }

    public void setIdEstanteria(Integer idEstanteria) {
        this.idEstanteria = idEstanteria;
    }

    public int getCapacidadEstanteria() {
        return capacidadEstanteria;
    }

    public void setCapacidadEstanteria(int capacidadEstanteria) {
        this.capacidadEstanteria = capacidadEstanteria;
    }

    public String getTipoEstanteria() {
        return tipoEstanteria;
    }

    public void setTipoEstanteria(String tipoEstanteria) {
        this.tipoEstanteria = tipoEstanteria;
    }

    public String getPosicionEstanteria() {
        return posicionEstanteria;
    }

    public void setPosicionEstanteria(String posicionEstanteria) {
        this.posicionEstanteria = posicionEstanteria;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEstanteria != null ? idEstanteria.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Estanteria)) {
            return false;
        }
        Estanteria other = (Estanteria) object;
        return !((this.idEstanteria == null && other.idEstanteria != null) || (this.idEstanteria != null && !this.idEstanteria.equals(other.idEstanteria)));
    }

    @Override
    public String toString() {
        return "org.mintic.tiendamibarrio.entidades.Estanterias[ idEstanteria=" + idEstanteria + " ]";
    }
    
}
