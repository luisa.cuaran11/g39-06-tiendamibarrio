package org.mintic.tiendamibarrio.servicios;

import java.util.List;
import java.util.Optional;
import org.mintic.tiendamibarrio.entidades.Facturacion;
import org.mintic.tiendamibarrio.interfaces.Operaciones;
import org.mintic.tiendamibarrio.repositorios.FacturacionRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service("facturacionService")
public class FacturacionServicios implements Operaciones<Facturacion>{
    
    @Autowired
    private FacturacionRepositorio tuRepo;

    @Override
    public List<Facturacion> consultar() {
        return tuRepo.obtenerTodos();
    }

    @Override
    public Boolean agregar(Facturacion miObjeto) {
        Facturacion temporal = tuRepo.save(miObjeto);
        return temporal != null;
    }

    @Override
    public Boolean eliminar(Integer llavePrimaria) {
        tuRepo.deleteById(llavePrimaria);
        return !tuRepo.existsById(llavePrimaria);
    }

    @Override
    public Facturacion buscar(Integer llavePrimaria) {
        return tuRepo.findById(llavePrimaria).get();
    }

    @Override
    public Boolean actualizar(Facturacion miObjeto) {
        Optional<Facturacion> objVerificar = tuRepo.findById(miObjeto.getIdFacturacion());
        if (objVerificar.isPresent()) {
            tuRepo.save(miObjeto);
            return true;
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Objeto no aceptado");
        }
    }

    @Override
    public Integer cantidadRegistros() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    
    
}
