package org.mintic.tiendamibarrio.repositorios;

import java.util.List;
import org.mintic.tiendamibarrio.entidades.Facturacion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface FacturacionRepositorio extends JpaRepository<Facturacion, Integer>{
    @Query("SELECT fac FROM Facturacion fac")
    public List<Facturacion> obtenerTodos();
}
